<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://mediusware.com
 * @since      1.0.0
 *
 * @package    Fattureincloud
 * @subpackage Fattureincloud/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Fattureincloud
 * @subpackage Fattureincloud/includes
 * @author     Mediusware <coredeveloper.2013@gmail.com>
 */
class Fattureincloud_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
